﻿import time
import re
import imaplib
import poplib
import quopri
import logging
import base64

import error

class MailReceiver(object):

    def __init__(self, host, port, ssl,
                                     login, password):
        self.num = 0
        self.host = host
        self.port = port
        self.ssl = ssl
        self.login = login
        self.password = password

    def close(self, delete=False):
        if delete:
            logging.debug('Delete message: {0}'.format(self.num))

class IMAPGet(MailReceiver):

    def __init__(self, *args, **kwargs):
        super(IMAPGet, self).__init__(*args, **kwargs)
        if self.ssl: 
            self.M = imaplib.IMAP4_SSL(self.host, self.port)
        else: 
            self.M = imaplib.IMAP4(self.host, self.port)

    def get_mails(self, check='ALL'):

        self.M.login(self.login, self.password)
        self.M.select('Inbox')
        typ, data = self.M.search(None, check)

        for num in data[0].split():
            typ, data = self.M.fetch(num, '(RFC822)')
            body = data[0][1]
            self.num = num
            yield body

    def close(self, delete=False):
        super(IMAPGet, self).close(delete)
        if delete:
            self.M.store(self.num, '+FLAGS', '\\Deleted')
        self.M.close()
        self.M.logout()


class POP3Get(MailReceiver):
    def __init__(self, *args, **kwargs):
        super(POP3Get, self).__init__(*args, **kwargs)
        if self.ssl: 
            self.M = poplib.POP3_SSL(self.host, self.port)
        else: 
            self.M = poplib.POP3(self.host, self.port)

    def get_mails(self, check='None'):
        self.M.user(self.login)
        self.M.pass_(self.password)
        response, lst, octets = self.M.list()
        for msgnum, msgsize in [i.split() for i in lst]:
            (resp, lines, octets) = self.M.retr(msgnum)
            body = "\n".join(lines)
            self.num = msgnum
            yield body

    def close(self, delete=False):
        super(POP3Get, self).close(delete)
        if delete:
            self.M.dele(self.num)
        self.M.quit()


class MailGet(object):
    def __init__(self, host, login, password, **kwargs):
        self.host = host
        self.login = login
        self.password = password
        self.ssl = kwargs.get('ssl', False)
        self.port = kwargs.get('port', 143)
        self.protocol = kwargs.get('protocol', 'imap')
        self.timeout = kwargs.get('timeout', 15)
        self.quopri_decode = kwargs.get('quopri_decode', False)
        self.base64 = kwargs.get('base64', False)
        check = kwargs.get('check', None)
        if check:
            self.check = '(TEXT "%s")' % check
        else:
            self.check = 'ALL'

    def _check_body(self, text, *signs):
        for sign in signs:
            if not sign.lower() in text.lower(): return False
        return True

    def _find_url(self, mail, regex, *signs):
        if self.base64:
            mail = base64.b64decode(letter)
        if self.quopri_decode:
            mail = quopri.decodestring(mail)
        logging.debug(''' Receiving mail:
                        ====================================================
                        %s
                        ====================================================
                      ''' % mail)
        if not self._check_body(mail, *signs): 
            return None
        url = re.findall(regex, mail, re.DOTALL)
        if url: 
            return url[0]

    def get_mail(self, regex, delete, *signs):
        if self.protocol == "imap":
            MailReceiver = IMAPGet
        elif self.protocol == "pop3":
            MailReceiver = POP3Get
        else:
            raise error.MailGetError('Unknow protocol: %s' % self.protocol)
        mail_receiver = MailReceiver(self.host, self.port, self.ssl,
                                        self.login, self.password)
        for mail in mail_receiver.get_mails():
            url = self._find_url(mail, regex, *signs)
            if url:
                mail_receiver.close(delete)
                return url

    def wait_mail(self, regex, trys, delete, *signs):
        for offer in xrange(trys):
            url = self.get_mail(regex, delete, *signs)
            if url:
                return url
            else:
                time.sleep(self.timeout)
def main():
    logging.basicConfig(level=logging.DEBUG)
    mail_login = "python4seo.net+admin"
    mail_password = "####"

    pop_imap = MailGet('imap.spaceweb.ru', mail_login, mail_password,
                        port=143, ssl=0, protocol="imap", 
                        quopri_decode=True, timeout=25, delete=True)

    approve = pop_imap.wait_mail('<a href="(http://mx.tumblr.com[^"]+)"', 1, False, "tumblr.com")
    print approve
if __name__ == '__main__':
    main()